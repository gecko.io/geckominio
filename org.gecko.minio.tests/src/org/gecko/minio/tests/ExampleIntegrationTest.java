/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved.
 * This program and the accompanying materials are made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * Contributors:
 * Data In Motion - initial API and implementation
 */
package org.gecko.minio.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Hashtable;

import org.gecko.core.tests.AbstractOSGiTest;
import org.gecko.core.tests.ServiceChecker;
import org.gecko.minio.MinioClient;
import org.gecko.minio.MinioClientException;
import org.gecko.minio.message.DeleteObjectsOutput;
import org.gecko.minio.message.ListAllMyBucketsResult;
import org.gecko.minio.message.ListBucketResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.cm.Configuration;


/**
 * <p>
 * This is a Minio Integration Test
 * </p>
 *
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
public class ExampleIntegrationTest extends AbstractOSGiTest {

    /**
     * Creates a new instance.
     *
     * @param bundleContext
     */
    public ExampleIntegrationTest() {
        super(FrameworkUtil.getBundle(ExampleIntegrationTest.class).getBundleContext());
    }

    /**
     * Here you can put everything you want to be executed before every test
     */
    @Override
    public void doBefore() {

    }

    /**
     * Here you can put everything you want to be executed after every test
     */
    @Override
    public void doAfter() {

    }

    /**
     * Here you can put your test
     *
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testBringUpClient() throws IOException {
        Dictionary<String, Object> properties = createConfiguration();

        // create a config admin configuration, that will be cleaned up after this test automatically
        Configuration configuration = createConfigForCleanup("MinioClient", "test", "?", properties);
        // get a service checker for this configuration
        ServiceChecker<MinioClient> clientChecker = (ServiceChecker<MinioClient>) getServiceCheckerForConfiguration(configuration);
        // wait, until the service is up an created one time
        MinioClient client = clientChecker.assertCreations(1, true).getTrackedService();
        assertEquals("test", client.getId());
    }

    /**
     * Here you can put your test
     *
     * @throws IOException
     * @throws MinioClientException
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testUpload() throws IOException, MinioClientException {
        Dictionary<String, Object> properties = createConfiguration();

        // create a config admin configuration, that will be cleaned up after this test automatically
        Configuration configuration = createConfigForCleanup("MinioClient", "test", "?", properties);
        // get a service checker for this configuration
        ServiceChecker<MinioClient> clientChecker = (ServiceChecker<MinioClient>) getServiceCheckerForConfiguration(configuration);
        // wait, until the service is up an created one time
        MinioClient client = clientChecker.assertCreations(1, true).getTrackedService();
        assertEquals("test", client.getId());

        try {
            client.getObject("example-bucket", "ExampleData.txt", "application/octet-stream");
            fail("Don't expect to have this file ExampleData.txt");
        } catch (MinioClientException e) {
            assertEquals(404, e.getResponseCode());
        }

        assertFalse(client.bucketExists("example-bucket"));
        client.createBucket("example-bucket");
        assertTrue(client.bucketExists("example-bucket"));
        assertFalse(client.bucketExists("example-bucket2"));

        ListAllMyBucketsResult listAllMyBucketsResult = client.listBuckets();
        assertTrue(listAllMyBucketsResult.getBuckets().stream().anyMatch(b -> "example-bucket".equals(b.getName())));

        ListBucketResult listObjectsEmpty = client.listObjects("example-bucket");
        assertTrue(listObjectsEmpty.getContents().isEmpty());

        assertFalse(client.objectExists("example-bucket", "ExampleData.txt"));
        URL fileUrl = getBundleContext().getBundle().getEntry("/data/ExampleData.txt");
        InputStream fileIS = fileUrl.openStream();
        assertNotNull(fileIS);
        client.putObject("example-bucket", "ExampleData.txt", "application/octet-stream", fileIS);
        assertTrue(client.objectExists("example-bucket", "ExampleData.txt"));

        ListBucketResult listObjectsExample = client.listObjects("example-bucket");
        assertTrue(listObjectsExample.getContents().stream().anyMatch(c -> "ExampleData.txt".equals(c.getKey())));

        client.deleteObject("example-bucket", "ExampleData.txt");
        ListBucketResult listObjectsExample2 = client.listObjects("example-bucket");
        assertTrue(listObjectsExample2.getContents().stream().noneMatch(c -> "ExampleData.txt".equals(c.getKey())));

        InputStream fileIS2 = fileUrl.openStream();
        assertNotNull(fileIS2);
        String fancyObjectKey = "E X+Ä.2 ?ß._-:;.txt";
        client.putObject("example-bucket", fancyObjectKey, "application/octet-stream", fileIS2);
        assertTrue(client.objectExists("example-bucket", fancyObjectKey));
        client.deleteObject("example-bucket", fancyObjectKey);
        assertFalse(client.objectExists("example-bucket", fancyObjectKey));

        InputStream fileIS3 = fileUrl.openStream();
        client.putObject("example-bucket", "file1", "application/octet-stream", fileIS3);
        InputStream fileIS4 = fileUrl.openStream();
        client.putObject("example-bucket", "file2", "application/octet-stream", fileIS4);
        InputStream fileIS5 = fileUrl.openStream();
        client.putObject("example-bucket", "file3", "application/octet-stream", fileIS5);

        DeleteObjectsOutput deleteObjectsOutput = client.deleteObjects("example-bucket", Arrays.asList(new String[] {"file1", "file2", "file3"}));
        assertEquals(3, deleteObjectsOutput.getDeleted().size());
        assertTrue(deleteObjectsOutput.getErrors().isEmpty());

        client.deleteBucket("example-bucket");
        assertFalse(client.bucketExists("example-bucket"));
    }

    /**
     * @throws IOException
     * @throws MinioClientException
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testEqualContent() throws IOException, MinioClientException {
        Dictionary<String, Object> properties = createConfiguration();

        // create a config admin configuration, that will be cleaned up after this test automatically
        Configuration configuration = createConfigForCleanup("MinioClient", "test", "?", properties);
        // get a service checker for this configuration
        ServiceChecker<MinioClient> clientChecker = (ServiceChecker<MinioClient>) getServiceCheckerForConfiguration(configuration);
        // wait, until the service is up an created one time
        MinioClient client = clientChecker.assertCreations(1, true).getTrackedService();
        assertEquals("test", client.getId());

        client.createBucket("example-bucket");

        URL fileUrl = getBundleContext().getBundle().getEntry("/data/ExampleData.txt");
        InputStream fileIS = fileUrl.openStream();
        assertNotNull(fileIS);

        // store original content
        ByteArrayOutputStream originalBOS = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        for (int length; (length = fileIS.read(buffer)) >= 0;) {
            originalBOS.write(buffer, 0, length);
        }
        originalBOS.close();
        fileIS.close();
        byte[] originalContent = originalBOS.toByteArray();

        // upload
        fileIS = fileUrl.openStream();
        client.putObject("example-bucket", "ExampleData.txt", "application/octet-stream", fileIS);
        assertTrue(client.objectExists("example-bucket", "ExampleData.txt"));

        // store remote content
        InputStream remoteContentIS = client.getObject("example-bucket", "ExampleData.txt", "application/octet-stream");

        ByteArrayOutputStream remoteBOS = new ByteArrayOutputStream();
        for (int length; (length = remoteContentIS.read(buffer)) >= 0;) {
            remoteBOS.write(buffer, 0, length);
        }
        remoteBOS.close();
        remoteContentIS.close();
        byte[] remoteContent = remoteBOS.toByteArray();

        // clean up
        client.deleteObject("example-bucket", "ExampleData.txt");
        client.deleteBucket("example-bucket");
        assertFalse(client.bucketExists("example-bucket"));

        if ( !Arrays.equals(originalContent, remoteContent)) {
            System.err.println("Original Content Length:" + originalContent.length);
            System.err.println("Remote Content Length:" + remoteContent.length);
            fail("original and remote content are not equal");
        }
    }


    /**
     * Here you can put your test
     *
     * @throws IOException
     * @throws MinioClientException
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testRemoveMany() throws IOException, MinioClientException {
        Dictionary<String, Object> properties = createConfiguration();

        // create a config admin configuration, that will be cleaned up after this test automatically
        Configuration configuration = createConfigForCleanup("MinioClient", "test", "?", properties);
        // get a service checker for this configuration
        ServiceChecker<MinioClient> clientChecker = (ServiceChecker<MinioClient>) getServiceCheckerForConfiguration(configuration);
        // wait, until the service is up an created one time
        MinioClient client = clientChecker.assertCreations(1, true).getTrackedService();
        assertEquals("test", client.getId());

        // try {
        // client.getObject("example-bucket", "ExampleData.txt", "application/octet-stream");
        // fail("Don't expect to have this file ExampleData.txt");
        // } catch (MinioClientException e) {
        // assertEquals(404, e.getResponseCode());
        // }
        //
        // assertFalse(client.bucketExists("example-bucket"));
        // client.createBucket("example-bucket");
        // assertTrue(client.bucketExists("example-bucket"));
        // assertFalse(client.bucketExists("example-bucket2"));
        //
        // ListAllMyBucketsResult listAllMyBucketsResult = client.listBuckets();
        // assertTrue(listAllMyBucketsResult.getBuckets().stream().anyMatch(b -> "example-bucket".equals(b.getName())));
        //
        // ListBucketResult listObjectsEmpty = client.listObjects("example-bucket");
        // assertTrue(listObjectsEmpty.getContents().isEmpty());
        //
        // assertFalse(client.objectExists("example-bucket", "ExampleData.txt"));
        // URL fileUrl = getBundleContext().getBundle().getEntry("/data/ExampleData.txt");
        // InputStream fileIS = fileUrl.openStream();
        // assertNotNull(fileIS);
        // client.putObject("example-bucket", "ExampleData.txt", "application/octet-stream", fileIS);
        // assertTrue(client.objectExists("example-bucket", "ExampleData.txt"));
        //
        // ListBucketResult listObjectsExample = client.listObjects("example-bucket");
        // assertTrue(listObjectsExample.getContents().stream().anyMatch(c -> "ExampleData.txt".equals(c.getKey())));
        //
        // client.deleteObject("example-bucket", "ExampleData.txt");
        // ListBucketResult listObjectsExample2 = client.listObjects("example-bucket");
        // assertTrue(listObjectsExample2.getContents().isEmpty());
        // assertTrue(listObjectsExample2.getContents().stream().noneMatch(c -> "ExampleData.txt".equals(c.getKey())));
        //
        // InputStream fileIS2 = fileUrl.openStream();
        // assertNotNull(fileIS2);
        // String fancyObjectKey = "E X+Ä.2 ?ß._-:;.txt";
        // client.putObject("example-bucket", fancyObjectKey, "application/octet-stream", fileIS2);
        // assertTrue(client.objectExists("example-bucket", fancyObjectKey));
        // client.deleteObject("example-bucket", fancyObjectKey);
        // assertFalse(client.objectExists("example-bucket", fancyObjectKey));
        //
        // InputStream fileIS3 = fileUrl.openStream();
        // client.putObject("example-bucket", "file1", "application/octet-stream", fileIS3);
        // InputStream fileIS4 = fileUrl.openStream();
        // client.putObject("example-bucket", "file2", "application/octet-stream", fileIS4);
        // InputStream fileIS5 = fileUrl.openStream();
        // client.putObject("example-bucket", "file3", "application/octet-stream", fileIS5);

        DeleteObjectsOutput deleteObjectsOutput = client.deleteObjects("example-bucket", Arrays.asList(new String[] {"file1", "file2", "file3"}));
        assertEquals(3, deleteObjectsOutput.getDeleted().size());
        assertTrue(deleteObjectsOutput.getErrors().isEmpty());

        client.deleteBucket("example-bucket");
        assertFalse(client.bucketExists("example-bucket"));
    }

    private Dictionary<String, Object> createConfiguration() throws IOException {
        Dictionary<String, Object> properties = new Hashtable<>();
        properties.put(MinioClient.CONFIG_KEY_NAME, "test");
        properties.put(MinioClient.CONFIG_KEY_PROTOCOL, "http");
        properties.put(MinioClient.CONFIG_KEY_TIMEOUT, Integer.valueOf(30));
        properties.put(MinioClient.CONFIG_KEY_PORT, 8100);
        properties.put(MinioClient.CONFIG_KEY_HOST, "devserv.dd.tragwerk-software.de");
        properties.put(MinioClient.CONFIG_KEY_ACCESSKEY, "administrator");
        properties.put(MinioClient.CONFIG_KEY_SECRETKEY, "administrator");
        return properties;
    }

}
